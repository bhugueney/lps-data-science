# Datasets DataNova

<https://datanova.laposte.fr/>

## La Poste

### Liste des services disponibles en bureaux de poste, agences postales et relais poste
Liste des points de contact du réseau postal français (bureaux de poste, agences postales et relais poste), incluant l'adresse, les coordonnées de géolocalisation, les principaux services associés (distributeur de billets, affranchissement libre-service, ...) et les équipements d'accessibilité.


### Liste des automates en bureaux de poste
Liste des automates présents en bureaux de poste, pour les clients ou pour les agents.


### Liste des boîtes aux lettres de rue France métropolitaine et DOM
Ce jeu de données fournit la liste des boîtes aux lettres de rue (BAL), boîtes aux lettres et boîtes aux lettres modulables, présentes en France métropolitaine et dans les DOM, incluant pour chaque BAL l’adresse et les coordonnées de géolocalisation de la BAL.


## Externe

### Historique des populations légales

Cette base propose les populations légales des communes de France métropolitaine aux recensements de la population de 1968, 1975, 1982, 1990, 1999, de 2006 à 2014. Pour les communes des départements d'outre-mer (hors Mayotte), de Saint-Barthélemy, de Saint-Martin et de Saint-Pierre-et-Miquelon, elles ne sont disponibles qu'à partir de 1990.


### Communes - France

Ce jeu de données fait partie du "référentiel géographique" Opendatasoft. Ces données sont utilisées dans nos processeurs et outils de la plateforme. Ces données ont été créées à partir de plusieurs sources de données différentes.


### Régions - France
Ce jeu de données fait partie du "référentiel géographique" Opendatasoft. Ces données sont utilisées dans nos processeurs et outils de la plateforme. Ces données ont été créées à partir de plusieurs sources de données différentes.


### Départements et Collectivités d'Outre-Mer - France
Ce jeu de données fait partie du "référentiel géographique" Opendatasoft. Ces données sont utilisées dans nos processeurs et outils de la plateforme. Ces données ont été créées à partir de plusieurs sources de données différentes.

### Fichier consolidé des Bornes de Recharge pour Véhicules Électriques (IRVE)
Depuis le décret n° 2017-26 du 12 janvier 2017, les données relatives à la localisation géographique et aux caractéristiques techniques des stations et des points de recharge de véhicules électriques ouverts au public doivent être publiées sur data.gouv.fr. Le format des données est défini par l’arrêté du 12 janvier 2017