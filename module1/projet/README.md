# Titre
Analyse des données du covid-19 (John Hopkins, Google, Oxford)

# Description rapide
Le but du projet est d'étudier 3 sources de données pour analyser la propagation du virus (source John Hopkins University), ainsi que les différentes attitudes des gouvernements en réaction (source Oxford).

Les données seront préparées sur dataiku avant l'analyse.

# Contexte

## Préparation des données

### Création des datasets

1. download recipe : Utilisez csse_covid_19_time_series comme nom de dossier et ajoutez les trois sources deaths, global et recovered : https://github.com/CSSEGISandData/COVID-19/blob/master/csse_covid_19_data/csse_covid_19_time_series. Il faut utiliser les lien raw comme adresses : 
   - https://raw.githubusercontent.com/CSSEGISandData/COVID-19/master/csse_covid_19_data/csse_covid_19_time_series/time_series_covid19_deaths_global.csv
   - https://raw.githubusercontent.com/CSSEGISandData/COVID-19/master/csse_covid_19_data/csse_covid_19_time_series/time_series_covid19_confirmed_global.csv
   - https://raw.githubusercontent.com/CSSEGISandData/COVID-19/master/csse_covid_19_data/csse_covid_19_time_series/time_series_covid19_recovered_global.csv

2. Créez un dataset pour chacune de ces trois sources

### dataset death

1. code recipe : Il faut commencer par transformer les colonnes de date en lignes. Pour cela utilisez un [code recipe](https://knowledge.dataiku.com/latest/courses/quick-start/data-scientist/code-and-code-recipes.html?highlight=python%20notebook#explore-the-python-recipe). Vous pourrez alors utiliser la méthode melt de pandas comme dans l'exemple suivant :

```python
# Compute recipe outputs from inputs
confirmed_pivoted_df = confirmed_df.melt(id_vars=["Country/Region", "Province/State", "Lat", "Long"], 
        var_name="Date", 
        value_name="Value")
```

2. prepare recipe : 
   - Parsez la date
   - Supprimez les colonnes Province/state, lat et long
   - Supprimez toutes les lignes pour lesquelles les pays sont invalides (ou mieux, remplacez les 13 pays incorrects par leurs [bons noms ISO](https://github.com/lukes/ISO-3166-Countries-with-Regional-Codes/blob/master/all/all.csv))

3. group recipe : Grouppez par Country/region et par date en calculant la somme de "Value"

### dataset recovered

1. code recipe : recréez la même étape que pour death (la fonction copie ne va pas donner de bons résultats pour un code recipe)
2. copier la prepare recipe et la group recipe en adaptant les inputs et les outpouts


## dataset confirmed

1. code recipe : recréez la même étape que pour death (la fonction copie ne va pas donner de bons résultats pour un code recipe)
2. copier la prepare recipe et la group recipe en adaptant les inputs et les outpouts

## All

1. join recipe : faites un join des 3 datasets précédents (sur Country/region et date) dans un dataset nommé "all". Faites en sorte d'obtenir les colonnes suivantes en sortie : 
   - (string) Country/Region
   - (string) Date
   - (bigint) conf_Value
   - (bigint) death_Value
   - (bigint) reco_Value

2. On va ajouter une nouvelle région correspondante au monde entier. Pour cela vous allez grouper le dataset précédent par Date en choisissant :
   - sum pour conf_value, death_value et reco_value
   - first pour Country/region : le premier pays permet de garder un colonne pour Country/region, vous pourrez ensuite avec une prepare recipe remplacer ce pays par "World"

3. stack recipe : utilisez cette recette pour empiler le dataset du 1. (all) et celui du 2. (world)

## Fin!

Vous pouvez maintenant télécharger le dataset avec la fonction export en le nommant final.csv :)


## Analyse des données

### Calcul d'indicateurs

   Pour le monde et pour la France (bonus : pour un pays sélectionné par
son nom / son identifiant) et pour un jour donné, indiquer les valeurs
de cas confirmés, décès, guérisons :
- pour le jour lui-même
- pour la semaine
- cumul depuis le début du jeu de données

Lorsque les données concernent un pays, (essayer d')indiquer les
valeurs des indices de mobilité et de mesures restrictives.

### Visualisation

   Pour le monde et pour la France (bonus : pour un pays sélectionné par
son nom / son identifiant)
   
   
#### Niveaux

 Sur un intervalle de temps paramétrable, visualiser la répartition
 des valeurs de cas confirmés, décès, guérisons.

 Lorsque les données concerne un pays, (essayer de) visualiser les valeurs des
indices de mobilité et de mesures restrictives.

#### Relations entre niveaux et indices de mobilité

 Sur un intervalle de temps paramétrable, visualiser les relations
 entre des valeurs de cas confirmés / décès / guérisons d'une part, et
 les indices de mobilité d'autre part.

####  Relations entre niveaux et mesures restrictives

 Sur un intervalle de temps paramétrable, visualiser les relations
 entre des valeurs de cas confirmés / décès / guérisons d'une part, et
 les mesures restrictives d'autre part.

    
#### Séries temporelles

Sur un intervalle de temps paramétrable, visualiser l'évolution des
cas confirmés / décès / guérisons.

(Essayer de) Représenter visuellement une (ou plusieurs ?) des mesures
restrictives (choix de celle(s)-ci paramétrable) sur l'intervalle de
temps considéré.


# Livrables

- un lien vers le notebook d'analyse. Ce notebook doit contenir une capture d'image du flow utilisé dans dataïku pour la préparation. N'oubliez pas de strurturer le notebook avec des titres pour le rendre plus facile à parcourir.

# Modalités

- par groupes de 2-3 personnes sans gros écart de niveau (une petite varaition de niveau est préférable tout de même pour une meilleure émulation)
