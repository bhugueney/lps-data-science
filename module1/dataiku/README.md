# Dataiku

## C'est quoi?

<https://www.dataiku.com/product/>

> La plateforme tout-en-un de science des données et d'apprentissage machine qui rassemble tout le monde pour avoir un impact transformateur sur les entreprises

![dataiku](dataiku.png)

- All-in-One Platform
- Collaborative Data Science

## Autres solutions tout-en-un sur le marché

<https://www.talend.com/products/data-fabric/>

<https://www.alteryx.com/fr/products/apa-platform>


## Autres solutions d'analyse des données

<https://www.tableau.com/> 

<https://powerbi.microsoft.com/fr-fr/>

<https://www.qlik.com/fr-fr>


## Data preparation

- [Recipes in DSS](https://knowledge.dataiku.com/latest/courses/basics/prepare-data/concept-recipe.html)
- [Join Recipe](https://knowledge.dataiku.com/latest/courses/lab-to-flow/join/join-summary.html)

## Exploring Datasets

### Connecting to and Exploring Data

- [Datasets in DSS](https://knowledge.dataiku.com/latest/courses/basics/create-dataset/concept-dataset.html)
- [Schema](https://knowledge.dataiku.com/latest/courses/basics/explore-data/concept-schema.html)
- [Storage Type and Meaning](https://knowledge.dataiku.com/latest/courses/basics/explore-data/concept-storage-meaning.html)
- [Sampling](https://knowledge.dataiku.com/latest/courses/basics/explore-data/concept-sampling.html)
- [Analyze](https://knowledge.dataiku.com/latest/courses/basics/explore-data/concept-analyze.html)
- [Connections](https://knowledge.dataiku.com/latest/courses/basics/create-dataset/concept-connection.html)

### Charts

- [Charts](https://knowledge.dataiku.com/latest/courses/basics/explore-data/concept-charts.html)

### Exploring Data in the Lab
- [The Lab](https://knowledge.dataiku.com/latest/courses/lab-to-flow/lab/lab-summary.html)
- [SQL Notebooks](https://knowledge.dataiku.com/latest/courses/dss-and-sql/sql-notebooks/concepts-summary.html)


## Reporting & Insights

- [Dashboards in DSS](https://knowledge.dataiku.com/latest/courses/lab-to-flow/reporting/concept-dashboards.html)

## Application






## Dataiku logo

<https://blog.dataiku.com/dataiku-logo>