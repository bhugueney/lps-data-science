# Projet Dataiku

## Datasets

### [CO2_and_Oil.csv](CO2_and_Oil.csv)
Comprend des informations annuelles sur les émissions de CO2 par habitant et la production de pétrole en térawatts-heures pour chaque pays. 

*Source: Our World in Data*

### [Urbanization_GDP_and_Population.csv](Urbanization_GDP_and_Population.csv)	
Pour chaque combinaison pays - année, comprend le pourcentage de personnes qui vivent dans des zones urbaines (par rapport aux zones rurales), le produit intérieur brut (PIB) par habitant et la population du pays cette année-là. 

*Source: Our World in Data*

### [Meat_and_Egg_Production.csv](Meat_and_Egg_Production.csv)	
Pour chaque combinaison pays - année, comprend la production de viande et d'œufs en tonnes, ainsi que l'approvisionnement en viande par personne en kg.
Comprend des informations annuelles sur les émissions de CO2 par habitant et la production de pétrole en térawattheures pour chaque pays.

*Source: Our World in Data*

## Instructions

1. Fusionnez les informations des trois tableaux en un seul. Nous recommandons d'utiliser le fichier CO2_and_Oil.csv comme base (tableau de gauche) pour fusionner les autres fichiers.
2. Limitez votre analyse aux données de 2008-2012 (inclus).
3. Certaines colonnes présentent les données totales du pays. Transformez ces colonnes en données par habitant. Vous n'aurez pas besoin des colonnes originales (y compris la population) pour une analyse plus approfondie.
4. Plusieurs colonnes des données par habitant comportent des lignes manquantes. Pour l'instant, remplissez ces valeurs avec des 0.
5. Les données fusionnées et préparées constituent maintenant une base à partir de laquelle nous pouvons effectuer des analyses plus approfondies. Elle doit comporter une colonne avec le nom du pays, une colonne indiquant l'année et 7 colonnes décrivant les statistiques sur ce pays pour cette année.
6. Dans l'ensemble de données fusionnées et préparées, créez une analyse univariée sur les 7 colonnes "descriptives", réparties par année.
7. À partir de l'ensemble de données fusionné et préparé, créez un nouvel ensemble de données qui agrège les valeurs des 7 colonnes "descriptives" de manière à ce qu'il y ait une seule ligne pour chaque pays qui contienne la valeur moyenne des colonnes "descriptives" sur les années 2008-2012.
8. Dans l'ensemble de données agrégées, créez une card sur une feuille de calcul statistique avec la matrice de corrélation des 7 colonnes "descriptives".

## Bonus

À partir de l'ensemble de données fusionnées et préparées, créez un nouvel ensemble de données qui a :

1. Une ligne par pays
2. Une colonne identifiant le pays auquel la ligne est associée
3. Une colonne par an de 2008 à 2012 qui indique le rang du pays dans la production de CO2 par habitant, 1 étant le rang du pays le plus producteur.
4. Une colonne par an de 2009 à 2012 qui indique l'évolution de la production de CO2 par rapport à l'année précédente
